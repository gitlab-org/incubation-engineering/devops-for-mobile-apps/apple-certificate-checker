#!/bin/ruby

require 'open3'
require 'date'

cmd = 'security find-certificate -p -c "Apple Worldwide Developer Relations Certification Authority" | openssl x509 -enddate -noout'

stdout, stderr, status = Open3.capture3(cmd)

if !stderr.empty?
  raise stderr
else
  _, exp_date_str = stdout.split('=')
  exp_date = DateTime.parse(exp_date_str)

  if exp_date < DateTime.now
    puts 'Certificate Expired'
    puts stdout
    raise 
  else
    puts "Certificate still valid, expires: #{exp_date_str}"
  end
end
